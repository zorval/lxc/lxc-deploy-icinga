Deploy Icinga with LXC
======================

----------------------------------------

## Needed

+ Debian 10 server
+ LXC already configured, on the server, see [lxc-config-base](https://framagit.org/zorval/lxc/lxc-config-base) repository for more informations.

----------------------------------------

## Create configuration file for icinga and start deploy

+ Copy `project/000-example` file to `project/icinga`
+ Edit values in `config/icinga` file
+ Edit usernames and passwords in `config/icinga` file
+ Start deploy by execute: `./auto-deploy-icinga icinga`

----------------------------------------

## Examples

After the deployment, in your *icinga-www* container , you can add your own hosts and checks.

You can see examples on the [icinga-check-examples](icinga-check-examples) directory.

----------------------------------------

## Links and documentations

+ [Icinga official - Documentation](https://www.icinga.com/docs)
+ [Icinga official - Getting started](https://www.icinga.com/docs/icinga2/latest/doc/02-getting-started)
+ [IcingaWeb official - Installation](https://www.icinga.com/docs/icingaweb2/latest/doc/02-Installation/)

