/**
 * Example host configuration file
 */

object Host "server.example.com" {
    import "generic-host"
    address = "server.example.com"          // or ip address
    vars.ssh_port = "22"

    /*
    * Define the hardware type
    * Groups are already available for the following matches:
    * - physical
    * - virtual
    * - lxc
    */
    vars.hw_type = "lxc"

    /*
    * If linux server, set the distribution here
    * Groups are already available for the following matches:
    * - debian
    * - ubuntu
    * - centos
    */
    vars.linux_version = "debian"

    /*
    * Enable or disable defaults checks
    * Those checks as available on the following repository:
    * https://framagit.org/zorval/scripts/check-nrpe
    */
    vars.enable_ssh                         = false
    vars.enable_nrpe_available_conntrack    = true
    vars.enable_nrpe_cpu                    = false
    vars.enable_nrpe_disk                   = true
    vars.enable_nrpe_load                   = false
    vars.enable_nrpe_memory                 = true
    vars.enable_nrpe_reboot                 = false
    vars.enable_nrpe_restart                = true
    vars.enable_nrpe_systemd                = true

    /****************************************/

    // Check website
    vars.http_vhosts["http-www"] = {
        http_uri = "/"
        http_ssl   = "1"
        http_address = "server.example.com" // Server FQDN or ip address
        http_vhost = "www.example.com"      // Service name - website FQDN
    }

    // Check cloud server
    vars.http_vhosts["http-cloud"] = {
        http_uri = "/"
        http_ssl   = "1"
        http_address = "server.example.com" // Server FQDN or ip address
        http_vhost = "cloud.example.com"      // Service name - website FQDN
    }

    // Check specific URL
    vars.http_vhosts["http-dummy"] = {
        http_uri = "/check"                 // Check /check URL
        http_ssl   = "1"
        http_address = "server.example.com" // Server FQDN or ip address
        http_vhost = "dummy.example.com"    // Service name - website FQDN
    }

    /*
    * Which group is notified ?
    * See user_example.conf file to create your user
    */
    vars.notification["mail"] = {
        groups = [ "example-grp" ]
    }

}

# vim: syntax=icinga2 ts=4 sw=4 sts=4 sr noet
